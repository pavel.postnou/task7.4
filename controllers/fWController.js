const express = require("express");
let fs = require("fs");
const emitter = require("../helpers/emitter.js")

exports.getWriteFile = function(request, response){
    fs.writeFileSync("hello.txt", "функция записи")
    response.send("<h1>Файл записан</h1>");
};

exports.getReadFile = function(request, response){
    response.send("<h1>Содержимое файла: </h1><p>" + fs.readFileSync("hello.txt", "utf8"));
};

exports.getWriteStream = function(request, response){
    let writeableStream = fs.createWriteStream("hello.txt");
    writeableStream.write("Привет мир!");
    writeableStream.end;
    response.send("<h1>Данные внесены</h1>");
};
exports.getReadStream = function(request, response){
   let readableStream = fs.createReadStream("hello.txt", "utf8")
   readableStream.on("data", function(chunk){ 
    response.send("<h1>Данные файла из потока </h1>"+chunk);
});

};

exports.postCopyFile =  function(request, response, next){
    let fileData = request.file.originalname;
    emitter.emit("changer",fileData)
    response.send("Успешно")
};