
const express = require("express");
const app = express();
let fs = require("fs");
const emitter = require("./helpers/emitter.js")
const userRouter = require("./routes/userRouter.js");
const homeRouter = require("./routes/homeRouter.js");
const adminRouter = require("./routes/adminRouter.js");
const fWRouter = require("./routes/fWRouter.js");
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));

app.use("/persons/user", userRouter);
app.use("/persons/admin", adminRouter);
app.use("/", homeRouter);
app.use("/persons/fileWorker", fWRouter)

emitter.on("changer", function (fileData) {

    let readableStream = fs.createReadStream(fileData, "utf8")
    let writeableStream = fs.createWriteStream("helloNew.txt")
    readableStream.pipe(writeableStream)

})

app.listen(3000);