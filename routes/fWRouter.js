const express = require("express");
const fWController = require("../controllers/fWController.js");
const fWRouter = express.Router();
const multer = require('multer');
const { EventEmitter } = require("stream");
const upload = multer();
const emitter = require("../helpers/emitter.js")

fWRouter.use("/writeSimple", fWController.getWriteFile);
fWRouter.use("/readSimple", fWController.getReadFile);
fWRouter.use("/writeStream", fWController.getWriteStream);
fWRouter.use("/readStream", fWController.getReadStream);
fWRouter.use("/writeFile", upload.single("file"),fWController.postCopyFile);
 
module.exports = fWRouter;
