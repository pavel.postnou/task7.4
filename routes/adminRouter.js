const express = require("express");
const adminController = require("../controllers/adminController.js");
const adminRouter = express.Router();
 
adminRouter.use("/create", adminController.postAdmin);
adminRouter.use("/:id", adminController.putAdmin);
adminRouter.use("/:id", adminController.getAdmin);
adminRouter.use("/:id", adminController.deleteAdmin);
 
module.exports = adminRouter;