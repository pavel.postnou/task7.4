const express = require("express");
const userController = require("../controllers/userController.js");
const userRouter = express.Router();
 
userRouter.use("/create", userController.postUser);
userRouter.use("/:id", userController.putUser);
userRouter.use("/:id", userController.getUser);
userRouter.use("/:id", userController.deleteUser);
 
module.exports = userRouter;