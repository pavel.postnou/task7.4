const admins = [];
 
module.exports = class Admin{
 
    constructor(name, id){
        this.name = name;
        this.id = id;
    }
    save(){
        admins.push(this);
    }
    static getAll(){
        return admins;
    }
}